import '../styles/Header.css';


function Header() {
    function shoping(){
        alert('Les produits sont en ruptures');
    }
    return (
        <>
            <div className="Imj-head">
                <div className='container'>
                    <div className="row">
                        <div className="col">
                            <h6>SUMMER FLASH SALE!</h6>
                            <p>Grab up to 60% off</p>
                            <p>on selected products</p>
                        </div>
                    </div>    
                    <div className="row"> 
                        <div className="col">      
                            <button className="dark" id="shop" onClick={shoping}>Shop Now</button>
                            <button className="white" id="watch"><i class="fa fa-play" aria-hidden="true"></i>Watch Trending</button>
                        </div>
                    </div>
                </div>
            </div>   
        </>
    )
}

export default Header;