import '../styles/ProductItem.css'

function ProductItem({ name, id, cover, prix}) {
	return (
        <>
            
                
                    <div key={id} className=' lmj-product-item'>
                        <img className='lmj-product-item-cover' src={cover} alt={`${name} cover`} />
                    </div>
                    <div className='Imj-text'>
                        <h4> {name} </h4>
                        <p> {prix} </p>
                    </div>
                    
                
            
        </>
	)
}

export default ProductItem