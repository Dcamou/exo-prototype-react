import {ProductList} from "../datas/ProductList"
import ProductItem from './ProductItem' // enfant de shoppingList
import '../styles/ShoppingList.css'

function ShoppingList() {
    return (
        <>
            
            <div className='lmj-product-list'>
                {ProductList.map(({name, id, cover, prix }) => (
					<div className="col-md-6 col-lg-4">
                        <ProductItem name={name} key={id} cover={cover} prix ={prix} /> 
                    </div>
                    ))}
			</div>
		</>
    )
}

export default ShoppingList