import Banner from './Banner';
import Header from './Header';
import Dropdowns from './Dropdowns';
import ShoppingList from './ShoppingList';
import Footer from './Footer';




function App() {
  return(
    <div>
      <Banner />
      <Header />
      <Dropdowns />
      <ShoppingList />
      <Footer />
    </div>
  )
}

export default App;