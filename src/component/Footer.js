import '../styles/Footer.css';

function Footer() {

    return (
        <>
            <div className="Footer">
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-4 col-lg-2 col-12'>
                            <h4>Shop</h4>
                            <p>Fashion</p>
                            <p>Qte Cards</p>
                            <p>Store Locatore</p>
                            <p>Refer a Friend</p>
                        </div>
                        <div className='col-md-4 col-lg-2 col-12'>
                            <h4>About</h4>
                            <p>Our Srory</p>
                            <p>Wholesale</p>
                            <p>Careers</p>
                            <p>Trends</p>
                            <p>Press</p>
                        </div>
                        <div className='col-md-4 col-lg-2 col-12'>
                            <h4>Help</h4>
                            <p>Contact us</p>
                            <p>FAQ</p>
                            <p>Acessibility</p>
                            
                        </div>
                        <div className='col-md-2 col-lg-1'><hr className='Imj-line'/></div>
                        <div className='col-md-8 col-lg-4 col-12'>
                            <h1>Newsletter</h1>
                            <p>Get the latest aboutus and sign up 10% of today. Never miss a singer promo.</p>
                            <div className="Imj-input">
                                <input placeholder="Enter email adress" aria-label="Enter email adress" aria-describedby="basic-addon2"/>
                                <button variant="outline-secondary" id="button-addon2">SUBSCRIBE</button>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col'>
                            <p><i class="fa fa-twitter" aria-hidden="true"></i><i class="fa fa-linkedin" aria-hidden="true"></i></p>
                        </div>
                        <hr />
                    </div>
                    <div className='row'>
                        <div className='col-md-3 col-lg-4 Imj-left'>
                            <p><span>C</span>2023 Seative Digital AllRights Reserved</p>
                        </div>
                        <div className='col-md-3 col-lg-4 Imj-right'>
                            <p>Terme of Service &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Privacy & Policy</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer;