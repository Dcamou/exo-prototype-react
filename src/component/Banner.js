import logo from '../assets/logo.png'
import '../styles/Banner.css';
import {Container, Nav, Navbar } from 'react-bootstrap';


function Banner() {
    
    return (
        <>
            <Navbar collapseOnSelect expand="md" className="bg-light">             
                <Navbar.Brand href="#home">
                    <a href='/'>
                        <img src={logo} alt="logo du site" height="70"/>
                    </a>
                </Navbar.Brand>
                <Container> 
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="m-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/">About</Nav.Link>
                        <Nav.Link href="/">Collection</Nav.Link>
                        <Nav.Link href="/">Blog</Nav.Link>
                        <Nav.Link href="/">Fashion</Nav.Link>
                    </Nav>
                    <Nav>
                        <Nav.Link href=""><i class="fa fa-search" aria-hidden="true"></i></Nav.Link>
                        <Nav.Link href="#deets"><i class="fa fa-users" aria-hidden="true"></i></Nav.Link>
                        <Nav.Link href="#deets">Cart</Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}

export default Banner;