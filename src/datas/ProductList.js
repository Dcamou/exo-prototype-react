import cookie from '../assets/cookie.jpg'
import alek from '../assets/alek.jpg'
import bram from '../assets/bram.jpg'
import dell from '../assets/dell.jpg'
import jul from '../assets/jul.jpg'
import nic from '../assets/nic.jpg'
import tar from '../assets/tar.jpg'
import tarn from '../assets/tarn.jpg'
import black from '../assets/blak.jpg'


export const ProductList = [
    {
        name: 'Full Slave Outfit',
        id: 1,
        prix: '$ 147.00',
        cover: cookie
    },
    {
        name: 'Coat',
        id: 2,
        prix: '$ 129.00',
        cover: alek
    },
    {
        name: 'Tops',
        id: 3,
        prix: '$ 149.00',
        cover: bram
    },
    {
        name: 'Half Slave Outfit',
        id: 4,
        prix: '$ 147.00',
        cover: dell
    },
    {
        name: 'Full Suit',
        id: 5,
        prix: '$ 189.00',
        cover: jul
    },
    {
        name: 'Red Velvet Dress',
        id: 6,
        prix: '$ 195.00',
        cover: nic
    },
    {
        name: 'Half Slave Outfit',
        id: 7,
        prix: '$ 147.00',
        cover: tar
    },
    {
        name: 'Hoodle',
        id: 8,
        prix: '$ 71.00',
        cover: tarn
    },
    {
        name: 'Black Glass',
        id: 9,
        prix: '$ 179.00',
        cover: black
    }
]